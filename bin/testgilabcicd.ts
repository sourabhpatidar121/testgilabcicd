#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { TestgilabcicdStack } from '../lib/testgilabcicd-stack';


const app = new cdk.App();

const stage = app?.node?.tryGetContext('stage');
const region = app?.node?.tryGetContext('region');
console.log({stage,region});

new TestgilabcicdStack(app, 'TestgilabcicdStack', {
  stackName:'TestgilabcicdStack'
});